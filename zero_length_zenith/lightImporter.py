# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing light sources.
#"""

import bpy
from mathutils import *
import PyHSPlasma as pl
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *

# might be worth checking the light import code from UPlasma. It's slightly more accurate than this one.
class LightImporter:

    def __init__(self, parent):
        self.parent = parent

    def importLight(self, sceneObjKey, lightKey):
        plLight = lightKey.object
        if lightKey.type == pl.plFactory.kOmniLightInfo:
            blLight = bpy.data.lamps.new(lightKey.name, "POINT")
        elif lightKey.type == pl.plFactory.kDirectionalLightInfo:
            blLight = bpy.data.lamps.new(lightKey.name, "SUN")
        elif lightKey.type == pl.plFactory.kSpotLightInfo:
            blLight = bpy.data.lamps.new(lightKey.name, "SPOT")
        elif lightKey.type == pl.plFactory.kLimitedDirLightInfo:
            # not really an area - more likely something like a sun/directional.
            # But that's how Korman handles it, should work fairly well.
            blLight = bpy.data.lamps.new(lightKey.name, "AREA")
        else:
            raise TypeError("Unknown lamp type %d" % lightKey.type)

        blObj = bpy.data.objects.new(sceneObjKey.name + "_LIGHT", blLight)

        if lightKey.type == pl.plFactory.kLimitedDirLightInfo:
            # since this is a bogus type of light, don't enable it
            blObj.hide_render = True

        ambient = [plLight.ambient.red, plLight.ambient.green, plLight.ambient.blue, plLight.ambient.alpha]
        diffuse = [plLight.diffuse.red, plLight.diffuse.green, plLight.diffuse.blue, plLight.diffuse.alpha]
        ambientIntensity = (ambient[0] + ambient[1] + ambient[2]) / 3 * ambient[3]
        diffuseIntensity = (diffuse[0] + diffuse[1] + diffuse[2]) / 3 * diffuse[3]
        finalColor = diffuse
        if ambientIntensity > diffuseIntensity:
            finalColor = ambient
        # this color may be over the Blender color range (HDR col for some reason), so remap it to the normal range,
        # and tweak the lamp's energy accordingly
        maxValue = max(finalColor[0], max(finalColor[1], finalColor[2]))
        if finalColor[3]:
            # divide color by energy. Dunno why you do that, but that's what Korman does, and yields good results :shrug:
            finalColor[0] /= finalColor[3]
            finalColor[1] /= finalColor[3]
            finalColor[2] /= finalColor[3]
        else:
            # this DOES happen... but we can't put a light's intensity to infinity ?...
            # Let's just ignore this light, and hope it turns out ok
            pass
        blLight.color = Color((finalColor[0], finalColor[1], finalColor[2]))
        blLight.energy = finalColor[3]
        hasShadows = False
        selfShadows = False
        shadowAttenDist = 10
        shadowMaxDist = 0
        shadowPower = 1
        for interface in sceneObjKey.object.interfaces:
            if interface and interface.object:
                if interface.type in (pl.plFactory.kPointShadowMaster, pl.plFactory.kDirectShadowMaster):
                    shadow = interface.object
                    hasShadows = True
                    selfShadows = shadow.getProperty(pl.plShadowMaster.kSelfShadow)
                    shadowAttenDist = shadow.attenDist
                    shadowMaxDist = shadow.maxDist
                    shadowPower = shadow.power
        blLight.shadow_method = ("NOSHADOW", "RAY_SHADOW")[hasShadows]
        if lightKey.type != pl.plFactory.kLimitedDirLightInfo:
            blLight.shadow_ray_samples = 8 # default setting to get good render results
            blLight.shadow_soft_size = 2   # same
        if lightKey.type not in (pl.plFactory.kDirectionalLightInfo, pl.plFactory.kLimitedDirLightInfo):
            # those values are completely inaccurate. While it's possible to export lights from Blender
            # and get a roughly ok result (like Korman does), it's too complicated to import Plasma lights
            # without spending hours tweaking the result and remap values (and I don't feel like it).
            if plLight.attenCutoff:
                blLight.distance = plLight.attenCutoff
                blLight.use_sphere = True
                blLight.falloff_type = 'INVERSE_SQUARE'
            else:
                # a lot of lights actually have 0 falloff, but still have limited reach, so ignore it
                pass
            if lightKey.type == pl.plFactory.kSpotLightInfo:
                # three new properties: falloff, spotInner, spotOuter. Falloff is unused as it's mostly a useless hack
                blLight.spot_size = plLight.spotOuter
                blLight.spot_blend = plLight.spotInner / plLight.spotOuter

        lyrKey = plLight.projection
        if self.parent.validKey(lyrKey):
            self.parent.matImporter.importProjectionLightLayer(lyrKey, blLight)

        self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerLights)

        # setup the Korman lamp
        kLamp = blLight.plasma_lamp
        kLamp.affect_characters = plLight.getProperty(pl.plLightInfo.kLPIncludesChars)
        kLamp.cast_shadows = hasShadows
        kLamp.shadow_self = selfShadows
        kLamp.shadow_falloff = shadowAttenDist
        kLamp.shadow_distance = shadowMaxDist
        kLamp.shadow_power = shadowPower * 100

        if self.parent.validKey(plLight.softVolume):
            def setLightVolume(softVolumeBlObject):
                kLamp.lamp_region = softVolumeBlObject
            self.parent.softVolumeImporter.registerSoftVolumeAndCallback(plLight.softVolume, setLightVolume)

        if lightKey.type == pl.plFactory.kLimitedDirLightInfo:
            blLight.size = plLight.width
            kLamp.size_height = plLight.height
            if plLight.width == plLight.height:
                blLight.shape = "SQUARE"
            else:
                blLight.shape = "RECTANGLE"
                # depth is mapped to size_y in Korman ? Weird. This means the dotted area in the 3D viewport is wrong.
                # Seems intentional, though.
                blLight.size_y = plLight.depth

        return blObj
