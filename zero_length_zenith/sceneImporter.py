# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing pages with all their objects, etc.
Puts objects on both the main scene and separate per-page scenes.

Plasma is a bit trickier than Blender, because it has an entity+modifier -like structure. Hence, a Plasma sceneObject with multiple modifiers can't be mapped 1:1 to a Blender object, because Blender objects are of different types.
Because of this, we will sometime generate multiple Blender objects for one single Plasma sceneObject (all attached to a master object).

Note that we first import all PRPs before setting up parenting and references - see importer.py for details.
#"""

import bpy, bmesh
from mathutils import *
from math import *
import PyHSPlasma as pl
from zero_length_zenith.utils import *

class SceneImporter:

    # layers on which we'll put different types of objects
    layerDefault        = (0,)
    layerColliders      = (1,)
    layerRegions        = (2,)
    layerAudio          = (3,)
    layerCameras        = (0, 4)
    layerSoftVolumes    = (5,)
    layerEmptiesMisc    = (6,)
    layerWorldBboxes    = (8,)
    layerSpaceTrees     = (9,)
    layerLights         = (0, 10)

    def __init__(self, parent):
        self.parent = parent
        self.bonesLength = .1 # display length of bones. Does not affect how the armature animates.
        self.mainScene = None
        self.curScene = None
        self.sceneNode = None
        self.sceneNodes = [] # sceneNodes are added to this list on each call to importScene
        self.currentlyImporting = False
        self.page = None
        self.poolObjectsToFinishSetup = []
        self.locationToScene = {}

    def importScene(self, location):
        self.page = self.parent.rmgr.FindPage(location)
        print("\nImporting page %s" % self.page.page)
        self.sceneNode = self.parent.rmgr.getSceneNode(location)
        if not self.sceneNode or self.page.page in ["BuiltIn", "Textures"] or (self.sceneNode and len(self.sceneNode.sceneObjects) == 0 and len(self.sceneNode.poolObjects) == 0):
            print("    ...Does not contain visible data. Ignoring.")
            return
        print("    %s objects" % len(self.sceneNode.sceneObjects))

        self.sceneNodes.append(self.sceneNode)

        # import the whole Age into the current scene
        self.mainScene = bpy.context.scene
        self.mainScene.game_settings.material_mode = "GLSL"
        self.mainScene.name = self.page.age

        # ...but also import the page currently being processed in its own scene.
        self.curScene = bpy.data.scenes.new(self.page.page)
        self.locationToScene[location] = self.curScene
        self.curScene.game_settings.material_mode = "GLSL"
        self.curScene.render.engine = "PLASMA_GAME"
        self.mainScene.layers = self.curScene.layers = [True] * 20
        self.curScene.world = self.mainScene.world # copy the main scene's world
        self.curScene.frame_start = 0 # Plazma animations start at 0
        self.mainScene.frame_start = 0
        self.curScene.render.fps_base = self.mainScene.render.fps_base
        self.curScene.render.fps = self.mainScene.render.fps

        # setup the Korman page
        pageSettings = self.mainScene.world.plasma_age.pages
        kormanPage = pageSettings.add()
        kormanPage.name = self.page.page
        kormanPage.seq_suffix = location.page
        kormanPage.local_only = hasFlags(location.flags, pl.plLocation.kLocalOnly)
        # auto_load is set in the .age file, and can only be set by the importer calling us.

        self.currentlyImporting = True

        # now, create and import all objects. References between objects won't be setup.
        for sceneObjKey in self.sceneNode.sceneObjects:
            self.parent.objectImporter.createObject(sceneObjKey)

        # also import supported pool objects
        for poolObjKey in self.sceneNode.poolObjects:
            if not self.parent.validKey(poolObjKey):
                continue
            objName = poolObjKey.name
            poolObj = poolObjKey.object

            if poolObjKey.type == pl.plFactory.kMsgForwarder:
                print("    Message fwder %s" % objName)
                blPoolObject = self.parent.getBlObjectFromKey(poolObj.forwardKeys[0])
                self.parent.registerObject(poolObjKey, blPoolObject.plasma_modifiers.animation_group)
                self.poolObjectsToFinishSetup.append((poolObjKey, blPoolObject))

            elif poolObjKey.type in (pl.plFactory.kDynaBulletMgr,
                                     pl.plFactory.kDynaDecalMgr,
                                     pl.plFactory.kDynaFootMgr,
                                     pl.plFactory.kDynaPuddleMgr,
                                     pl.plFactory.kDynaRippleMgr,
                                     pl.plFactory.kDynaRippleVSMgr,
                                     pl.plFactory.kDynaTorpedoMgr,
                                     pl.plFactory.kDynaTorpedoVSMgr,
                                     pl.plFactory.kDynaWakeMgr):
                print("    Decal %s, type %s" % (objName, poolObjKey.type))

                # Create managers.
                # (We do it in both scenes, just so the modifier is always valid no matter which scene is selected)
                kDecalMgrs = (self.curScene.plasma_scene.decal_managers.add(), self.mainScene.plasma_scene.decal_managers.add())
                matKey = poolObj.matRTShade if self.parent.validKey(poolObj.matRTShade) else poolObj.matPreShade
                image = None
                blend = "kBlendAlpha"
                if self.parent.validKey(matKey):
                    mat = matKey.object
                    # let's just assume there is only 1 layer, and it's perfectly setup...
                    lyrKey = self.parent.matImporter.getBaseLayerKey(mat.layers[0]) # this could qualify for inclusion in the MatImporter... But MatImporter is a mess already
                    if self.parent.validKey(lyrKey):
                        lyr = lyrKey.object
                        if hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendAdd):
                            blend = "kBlendAdd"
                        elif hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendMult):
                            blend = "kBlendMult"
                        elif hasFlags(lyr.state.blendFlags, pl.hsGMatState.kBlendMADD):
                            blend = "kBlendMADD"
                        if self.parent.validKey(lyr.texture):
                            lyrTexKey = lyr.texture
                            lyrTex = lyrTexKey.object
                            # Assert dominance by assuming it's a plMipmap and not even bother checking.
                            image = self.parent.imageImporter.getImage(lyrTexKey)
                decalType = "footprint_dry"
                if poolObjKey.type == pl.plFactory.kDynaFootMgr:
                    decalType = "footprint_wet" if poolObj.waitOnEnable else "footprint_dry"
                elif poolObjKey.type == pl.plFactory.kDynaPuddleMgr:
                    decalType = "puddle"
                elif poolObjKey.type in (pl.plFactory.kDynaRippleMgr, pl.plFactory.kDynaRippleVSMgr):
                    decalType = "ripple"
                else:
                    # Korman source hints this will probably be "wake", "bullet", "torpedo". But for now, don't try it.
                    print("WARNING: don't know what decal mgr type %d corresponds to." % poolObjKey.type)

                for kMgr in kDecalMgrs:
                    kMgr.name = poolObjKey.name
                    kMgr.image = image
                    kMgr.blend = blend
                    kMgr.decal_type = decalType
                    kMgr.length = poolObj.scale.X * 100
                    kMgr.width = poolObj.scale.Y * 100
                    kMgr.intensity = poolObj.intensity * 100
                    kMgr.life_span = poolObj.lifeSpan
                    if hasattr(poolObj, "wetLength"):
                        kMgr.wet_time = poolObj.wetLength

                # Now ensure target objects receive those decals.
                for targetKey in poolObj.targets:
                    blTargetObject = self.parent.getBlObjectFromKey(targetKey)
                    blTargetObject.select = True
                    bpy.context.scene.objects.active = blTargetObject
                    bpy.ops.object.plasma_modifier_add(types="decal_receive")
                    blTargetObject.select = False
                    kormanMod = blTargetObject.plasma_modifiers.decal_receive
                    mgr = kormanMod.managers.add()
                    mgr.name = poolObjKey.name

                if decalType in ("ripple", "puddle"):
                    for kMgr in kDecalMgrs:
                        for notifiedKey in poolObj.notifies:
                            wetMgr = kMgr.wet_managers.add()
                            wetMgr.name = notifiedKey.name

        # import space trees if this was asked
        if self.parent.config["importBounds"]:
            for dspanKey in self.parent.rmgr.getKeys(location, pl.plFactory.kDrawableSpans):
                if not self.parent.validKey(dspanKey):
                    continue
                dspan = dspanKey.object
                tree = dspan.spaceTree
                if not tree:
                    # fan content usually skips the space tree entirely
                    continue
                root = tree.getRoot()

                def importTreeNode(node, parentBlObj):
                    bm = bmesh.new()
                    mesh = bpy.data.meshes.new(dspanKey.name + "_" + str(node.getLeafIndex()))
                    createBBoxVertices(bm, node.bounds)
                    bm.to_mesh(mesh)
                    bm.free()
                    del bm
                    nodeBlObj = bpy.data.objects.new(mesh.name, mesh)
                    self.parent.sceneImporter.appendObjectToScenes(nodeBlObj, SceneImporter.layerSpaceTrees)
                    if parentBlObj:
                        nodeBlObj.parent = parentBlObj
                    nodeBlObj.matrix_world = Matrix() # just in case...
                    if not hasFlags(node.flags, pl.plSpaceTreeNode.kIsLeaf): # otherwise the node will list itself as its child... go figure.
                        for childIndex in node.getChildren():
                            importTreeNode(tree.getNode(childIndex), nodeBlObj)

                importTreeNode(root, None)

        self.currentlyImporting = False

    def appendObjectToScenes(self, blObject, layerIds):
        """Link the given blObject to both the main scene, and the currently processed PRP's scene.
        The object will be put on the given layerIds on each scene."""
        if not self.currentlyImporting:
            raise RuntimeError("Cannot add object to scene: no scene currently processed.")
        layers = [False] * 20
        for id in layerIds:
            layers[id] = True
        self.mainScene.objects.link(blObject).layers = layers
        self.curScene.objects.link(blObject).layers = layers

        # also setup Korman object info
        plasmaObject = blObject.plasma_object
        plasmaObject.enabled = True
        plasmaObject.page = self.page.page

    def appendObjectToScenesWithPlLocation(self, blObject, layerIds, plLocation):
        """Link the given blObject to both the main scene, and the scene with the given plLocation.
        The object will be put on the given layerIds on each scene.
        Should only be used after all scenes have been created.
        If the target scene could not be found, a dummy scene will be created for the object."""
        layers = [False] * 20
        for id in layerIds:
            layers[id] = True
        self.mainScene.objects.link(blObject).layers = layers
        targetScene = self.locationToScene.get(plLocation)
        if not targetScene:
            # ah, dammit, we don't know about this location. Create or get a scene with the plLocation's description as name...
            locStr = str(plLocation)
            print("WARNING - Object %s should be added to scene with location %s, which does not exist."
                % (blObject.name, locStr))
            targetScene = bpy.data.scenes.new(locStr)
            self.locationToScene[plLocation] = targetScene
        targetScene.objects.link(blObject).layers = layers

        # also setup Korman object info
        plasmaObject = blObject.plasma_object
        plasmaObject.enabled = True
        plasmaObject.page = self.page.page

    def rebuildHierarchy(self):
        # parent objects (only doable once all objects are imported as object hierarchy is reversed in Blender)
        for childKey, parentKey in self.parent.objectParents.items():
            if not self.parent.validKey(parentKey):
                continue
            childObj = self.parent.getBlObjectFromKey(childKey)
            parentObj = self.parent.getBlObjectFromKey(parentKey)
            if not childObj or not parentObj or childObj.parent: # ignore if one of the object wasn't imported, or if the object already has a parent (armature)
                continue

            childObj.parent = parentObj

    def setupReferences(self):
        # finish setting up references for pool objects we handle ourself
        for poolObjKey, blPoolObject in self.poolObjectsToFinishSetup:
            if poolObjKey.type == pl.plFactory.kMsgForwarder:
                blPoolObject.select = True
                bpy.context.scene.objects.active = blPoolObject
                bpy.ops.object.plasma_modifier_add(types="animation_group")
                blPoolObject.select = False
                kormanMod = blPoolObject.plasma_modifiers.animation_group
                for childAnimKey in poolObjKey.object.forwardKeys:
                    child = kormanMod.children.add()
                    child.child_anim = self.parent.getBlObjectFromKey(childAnimKey)
