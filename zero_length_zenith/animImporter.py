# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing object animations.

TODO:
- figure out keyframe tangents
- add more applicators
- animation events ?
- message forwarders
#"""

import bpy, math
from mathutils import *
import PyHSPlasma as pl
from zero_length_zenith.utils import *
from typing import Optional, Callable

class AnimImporter:

    def __init__(self, parent):
        self.parent = parent
        self.animationChannels = {} # maps a channel name to a scene object key
        self.blObjAnimationChannels = {} # maps a Blender object to a channel name
        self.masterModsToImport = [] # list of (sceneObjKey, agMasterModKey)
        self.version = None

    def registerChannel(self, anim, sceneObjKey, blObj):
        self.animationChannels[anim.channelName] = sceneObjKey
        self.blObjAnimationChannels[blObj] = anim.channelName

    def registerMasterMod(self, sceneObjKey, modifierKey):
        self.masterModsToImport.append((sceneObjKey, modifierKey))

    @staticmethod
    def getOrCreateFCurve(fcurves, curveName, index):
        fcurve = [curve for curve in fcurves if curve.data_path == curveName and curve.array_index == index]
        if len(fcurve) > 0:
            return fcurve[0]
        return fcurves.new(curveName, index)

    def importScalarController(self, controller, blAction, curveName, transform: Optional[Callable] = None):
        keyframes = controller.keys[0]
        frameMultiplier = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base
        blFCurve = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, 0)
        for keyframe in keyframes:
            frame = keyframe.frameTime * frameMultiplier
            value = keyframe.value
            if transform is not None:
                value = transform(value)
            blKf = blFCurve.keyframe_points.insert(frame, value)

            forceLinear = False
            if forceLinear:
                blKf.handle_right = Vector((frame, value))
                blKf.handle_left = Vector((frame, value))
            # else handles are already positioned correctly

            self.parent.endFrame = max(self.parent.endFrame, frame)

        return (blFCurve,)

    def importSimpleController(self, controller, blAction, curveName):
        keyframes = controller.keys[0]
        frameMultiplier = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base
        blFCurveX = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, 0)
        blFCurveY = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, 1)
        blFCurveZ = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, 2)
        for keyframe in keyframes:
            frame = keyframe.frameTime * frameMultiplier
            # If the keyframe is a scale keyframe, then 'value' is not the hsVector3 - it's actually a (hsVector3, hsQuat).
            # The quat is there to allow scaling on something else than a local axis - for instance, one of the world axes.
            # We are NOT going to import this, period. Non local scaling should never be done, ever.
            # Unfortunately, we're speaking about artists here...
            # Anyway. Just assume the scaling is done on local axes.
            actualValue = keyframe.value[0] if controller.keys[1] in (pl.hsKeyFrame.kBezScaleKeyFrame, pl.hsKeyFrame.kScaleKeyFrame) else keyframe.value
            blKfX = blFCurveX.keyframe_points.insert(frame, actualValue.X)
            blKfY = blFCurveY.keyframe_points.insert(frame, actualValue.Y)
            blKfZ = blFCurveZ.keyframe_points.insert(frame, actualValue.Z)

            forceLinear = False
            if forceLinear:
                blKfX.handle_right = Vector((frame, actualValue.X))
                blKfX.handle_left = Vector((frame, actualValue.X))
                blKfY.handle_right = Vector((frame, actualValue.Y))
                blKfY.handle_left = Vector((frame, actualValue.Y))
                blKfZ.handle_right = Vector((frame, actualValue.Z))
                blKfZ.handle_left = Vector((frame, actualValue.Z))
            # else handles are already positioned correctly

            self.parent.endFrame = max(self.parent.endFrame, frame)

        return (blFCurveX, blFCurveY, blFCurveZ)

    def importCompoundController(self, controller, blAction, curveName):
        x = controller.X
        y = controller.Y
        z = controller.Z
        frameMultiplier = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base
        allCurves = []
        for i, axisController in enumerate([x,y,z]):
            if not axisController:
                continue
            blFCurve = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, i)
            for keyframe in axisController.keys[0]:
                frame = keyframe.frameTime * frameMultiplier
                value = keyframe.value
                blKf = blFCurve.keyframe_points.insert(frame, value)

                forceLinear = False
                if forceLinear:
                    blKf.handle_right = Vector((frame, value))
                    blKf.handle_left = Vector((frame, value))
                # else handles are already positioned correctly

                self.parent.endFrame = max(self.parent.endFrame, frame)
            allCurves.append(blFCurve)

        return allCurves

    def importQuatController(self, controller, blAction, curveName):
        # Just a SimpleController with an extra channel... Unfortunately this will NOT allow us to correctly playback the animation.
        # We're running in the same stupid limitation that Unity has - namely, both softwares/animators interpolate quaternion XYZW
        # components SEPARATELY instead of doing a spherical lerp of the quat itself.
        # Well, there is this: https://developer.blender.org/D1929 but apparently it's going nowhere.
        # This is absolutely baffling. I'm tempted to give in to another long French rant, but for your sake I'll hold it in.
        keyframes = controller.keys[0]
        frameMultiplier = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base
        blFCurveW = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, 0)
        blFCurveX = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, 1)
        blFCurveY = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, 2)
        blFCurveZ = AnimImporter.getOrCreateFCurve(blAction.fcurves, curveName, 3)
        for keyframe in keyframes:
            frame = keyframe.frameTime * frameMultiplier
            actualValue = keyframe.value
            blKfX = blFCurveX.keyframe_points.insert(frame, actualValue.X)
            blKfY = blFCurveY.keyframe_points.insert(frame, actualValue.Y)
            blKfZ = blFCurveZ.keyframe_points.insert(frame, actualValue.Z)
            blKfW = blFCurveW.keyframe_points.insert(frame, actualValue.W)

            forceLinear = True
            if forceLinear:
                blKfX.handle_right = Vector((frame, actualValue.X))
                blKfX.handle_left = Vector((frame, actualValue.X))
                blKfY.handle_right = Vector((frame, actualValue.Y))
                blKfY.handle_left = Vector((frame, actualValue.Y))
                blKfZ.handle_right = Vector((frame, actualValue.Z))
                blKfZ.handle_left = Vector((frame, actualValue.Z))
                blKfW.handle_right = Vector((frame, actualValue.W))
                blKfW.handle_left = Vector((frame, actualValue.W))
            # else handles are already positioned correctly

            self.parent.endFrame = max(self.parent.endFrame, frame)

        return (blFCurveX, blFCurveY, blFCurveZ, blFCurveW)

    def importAnimations(self):
        for sceneObjKey, masterModKey in self.masterModsToImport:
            if not self.parent.validKey(masterModKey):
                continue
            print("    Master %s" % sceneObjKey.name)
            masterMod = masterModKey.object
            for animKey in masterMod.privateAnims:
                if not self.parent.validKey(animKey):
                    continue
                isAtc = True
                isGlobalAnim = False
                if animKey.type != pl.plFactory.kATCAnim:
                    print("WARNING: animation type %d not fully supported yet !" % animKey.type)
                    isAtc = False
                if animKey.type == pl.plFactory.kAgeGlobalAnim:
                    isGlobalAnim = True
                anim = animKey.object
                self.version = animKey.location.version

                for applicator in anim.applicators:
                    if isinstance(applicator, pl.plMatrixChannelApplicator):
                        blObj, blAction, boneDataPathPrefix, bone = self.getOrCreateAction(animKey, applicator)
                        if bone is not None:
                            print("WARNING: bone transform animations cannot be reliably imported. Skipping...")
                            # Reasoning behind this:
                            # - Bones in Plasma are just empty objects. Animation value are relative to the parent (l2p) as usual.
                            # - Bones in Blender require being part of an armature. They are animated from their REST position.
                            # - So far, ZLZ positions armatures using the transform for the first bone in the bone chain
                            # Because of this, we run into all sorts of issues with animations being expressed in the wrong space,
                            # which mathematically aren't really fixable without moving the armature's origin to the world origin -
                            # which would look ugly and dumb in case it's also parented to another non-bone empty.
                            # Instead of trying to shuffle objects, offset keyframe values and screw with bones animated using
                            # euler keyframes (yes T_T), we have a much more efficient and simple-to-implement alternative:
                            # Not giving a fuck.
                            # We'll let the user figure it out using PRPshop. You a user ? Yes ? Get to work.
                            # (Okay, if you intend to import and fix Kemo, then I'll allow you to shed tears of blood, at least.)
                            continue
                        channel = applicator.channel
                        controller = channel.controller

                        affine = channel.affine
                        if affine:
                            """
                            Mmhkay, this is important !
                            When we import objects, we let Blender figure out pos/rot/scale from the coordinate interface's matrix.
                            This works, but Blender only picks ONE of several valid combinations - not always the correct one...
                            Rotation of 180°, for instance, is also equivalent to a non-uniform negative scaling...
                            And, as you would expect, Cyan's artists never learnt that you should NEVER EVER scale objects if you can avoid it.
                            So they like to leave objects with non-one, non-uniform, negative or partially negative scaling. Booo !

                            Fortunately, this is usually not a problem for regular objects. This is however an issue for some animated objects,
                            like the butterflies in Relto. Those objects have rotation animated, but not scale. This causes issues because
                            rotation and scale are supposed to work together.
                            Solution: animated objects are repositioned manually using the affine parts of the animation.
                            Oh, and be grateful Blender has the exact same space as Plasma. Otherwise that would have been bloody.
                            #"""
                            t = Vector((affine.T.X, affine.T.Y, affine.T.Z))
                            r = Quaternion((affine.Q.W, affine.Q.X, affine.Q.Y, affine.Q.Z))
                            s = Vector((affine.K.X, affine.K.Y, affine.K.Z))

                            if affine.F < 0:
                                # We have at least one component of the scale vector which is negative.
                                # Hmmm... Simply assume we have to negate the whole scale.
                                # This appears to be working.
                                s = -s

                            setLocalXForm(blObj, t, r, s)

                        pos, rot, sca = None, None, None
                        if isinstance(controller, pl.plTMController):
                            pos, rot, sca = controller.pos, controller.rot, controller.scale
                        elif isinstance(controller, pl.plCompoundController):
                            pos, rot, sca = controller.X, controller.Y, controller.Z
                        else:
                            print("WARNING: animation controller %s not supported!" % controller.__class__.__name__)
                            continue

                        importedFCurves = []
                        if pos:
                            if isinstance(pos, pl.plSimplePosController):
                                importedFCurves.extend(self.importSimpleController(pos.position, blAction, boneDataPathPrefix + "location"))
                            elif isinstance(pos, (pl.plSimplePosController, pl.plLeafController)):
                                importedFCurves.extend(self.importSimpleController(pos, blAction, boneDataPathPrefix + "location"))
                            elif isinstance(pos, (pl.plCompoundPosController, pl.plCompoundController)):
                                importedFCurves.extend(self.importCompoundController(pos, blAction, boneDataPathPrefix + "location"))
                            else:
                                print("WARNING: animation pos controller %s not supported !" % pos.__class__.__name__)
                        if rot:
                            if isinstance(rot, pl.plSimpleRotController) and isinstance(rot.rot, pl.plQuatController):
                                importedFCurves.extend(self.importQuatController(rot.rot, blAction, boneDataPathPrefix + "rotation_quaternion"))
                                blObj.rotation_mode = "QUATERNION"
                            elif isinstance(rot, pl.plLeafController) and rot.type in ( \
                                    pl.hsKeyFrame.kQuatKeyFrame,
                                    pl.hsKeyFrame.kCompressedQuatKeyFrame32,
                                    pl.hsKeyFrame.kCompressedQuatKeyFrame64):
                                importedFCurves.extend(self.importQuatController(rot, blAction, boneDataPathPrefix + "rotation_quaternion"))
                                if bone is None:
                                    # if this is not a bone animation, set the object to use quat rotation.
                                    blObj.rotation_mode = "QUATERNION"
                            elif isinstance(rot, (pl.plCompoundRotController, pl.plCompoundController)):
                                importedFCurves.extend(self.importCompoundController(rot, blAction, boneDataPathPrefix + "rotation_euler"))
                            else:
                                print("WARNING: animation rot controller %s not supported !" % rot.__class__.__name__)
                        if sca:
                            if isinstance(sca, pl.plSimpleScaleController):
                                importedFCurves.extend(self.importSimpleController(sca.value, blAction, boneDataPathPrefix + "scale"))
                            elif isinstance(sca, pl.plLeafController):
                                importedFCurves.extend(self.importSimpleController(sca, blAction, boneDataPathPrefix + "scale"))
                            else:
                                print("WARNING: animation scale controller %s not supported !" % sca.__class__.__name__)

                        self.setupKormanModifier(blObj, isAtc, isGlobalAnim, anim, importedFCurves)

                    elif isinstance(applicator, pl.plLightAmbientApplicator) \
                            or isinstance(applicator, pl.plLightDiffuseApplicator):
                        blObj, blAction, boneDataPathPrefix, bone = self.getOrCreateAction(animKey, applicator)
                        channel = applicator.channel
                        controller = channel.controller

                        importedFCurves = []
                        if isinstance(controller, pl.plSimplePosController):
                            importedFCurves.extend(self.importSimpleController(controller.position, blAction, boneDataPathPrefix + "color"))
                        elif isinstance(controller, pl.plLeafController):
                            importedFCurves.extend(self.importSimpleController(controller, blAction, boneDataPathPrefix + "color"))
                        elif isinstance(controller, (pl.plCompoundPosController, pl.plCompoundController)):
                            importedFCurves.extend(self.importCompoundController(controller, blAction, boneDataPathPrefix + "color"))
                        else:
                            print("WARNING: light animation controller %s not supported !" % controller.__class__.__name__)
                            continue

                        self.setupKormanModifier(blObj, isAtc, isGlobalAnim, anim, importedFCurves)

                    elif isinstance(applicator, pl.plSoundVolumeApplicator):
                        blObj, blAction, boneDataPathPrefix, bone = self.getOrCreateAction(animKey, applicator)
                        channel = applicator.channel
                        controller = channel.controller

                        # Korman expects volume percentage, so transform keyframes as appropriate
                        convertVolume = lambda x: 10 ** (x / 20.0) * 100

                        importedFCurves = []
                        if isinstance(controller, pl.plScalarController) or isinstance(controller, pl.plLeafController):
                            importedFCurves.extend(self.importScalarController(controller, blAction,
                                boneDataPathPrefix + "plasma_modifiers.soundemit.sounds[{}].volume".format(applicator.index), convertVolume))
                        else:
                            print("WARNING: sound animation controller %s not supported !" % controller.__class__.__name__)
                            continue

                        self.setupKormanModifier(blObj, isAtc, isGlobalAnim, anim, importedFCurves)

                    elif isinstance(applicator, pl.plLightSpecularApplicator):
                        print("WARNING: light specular color cannot be animated in Blender !")

                    elif isinstance(applicator, pl.plParticlePPSApplicator):
                        print("WARNING: particle emitters not supported yet, no anim for them !")

                    else:
                        print("WARNING: animation applicator %s not supported !" % applicator.__class__.__name__)

    def getOrCreateAction(self, animKey, applicator):
        """Creates (or retrieves if already created) the action for an object.
        Returns (animated Blender object, action, bone data_path prefix, bone)."""

        # Mmhkay, another complex problem.
        # We want each animated object to have 1 assigned action.
        # We name each action using the animation channel name (obvious and means we rely on Blender to keep track of those for us).
        # But, actions are assigned to either the object itself (transform...), or its data (light intensity...).
        # Thus, our action name may be postfixed with "_data".
        # On top of that, bone transform animations must be placed on the ARMATURE itself, NOT the Blender bone,
        # NOR the Blender object we're using to hold other stuff such as light or sound sources.
        # In that case, our animation is postfixed with "_armature".
        # Other non-transform animations MUST be placed on the Blender object holding other stuff (light/sound).
        # Cherry on top: Korman expects an anim to use all fcurves from the action. Plasma doesn't enforce it.
        # BUT, we'll ignore that last issue, because 1. we don't have an alternative, and 2. fuck it.
        # UPDATE: bone transform animations cannot be reliably imported. Oh well.
        # I'm not removing the bone stuff from this method because I'm lazy. Feel free to poke at it.

        attachToData = isinstance(applicator, pl.plLightAmbientApplicator) or isinstance(applicator, pl.plLightDiffuseApplicator)

        actionName = applicator.channelName
        if attachToData: # don't mix this in the main action
            actionName += "_data"

        targetObjKey = self.animationChannels[applicator.channelName]
        print("        %s, on object %s" % (animKey.name, targetObjKey.name))
        targetBlenderObject = self.parent.getBlObjectFromKey(targetObjKey)
        targetAnimatedData = targetBlenderObject.data if attachToData else targetBlenderObject
        boneDataPathPrefix = ""

        bone = None
        if targetObjKey in self.parent.drawImporter.bonesToReconstruct and isinstance(applicator, pl.plMatrixChannelApplicator):
            # armature animations: this is the special case where we use the action for another object.
            targetAnimatedData = targetBlenderObject = self.parent.drawImporter.boneToArmature[targetObjKey]
            actionName = targetBlenderObject.name + "_armature"
            boneDataPathPrefix = "pose.bones[\"{}\"].".format(targetObjKey.name)
            # also fetch the bone itself. We're gonna need it to transform the anim from local space to rest space, %#*§.
            bone = targetBlenderObject.data.bones[targetObjKey.name]
            attachToData = False # just in case

        # get (or create) the correct action
        blAction = bpy.data.actions.get(actionName)
        if not blAction:
            blAction = bpy.data.actions.new(actionName)

        # ensure it's assigned to the object
        blAnimationData = targetAnimatedData.animation_data
        if not blAnimationData:
            blAnimationData = targetAnimatedData.animation_data_create()
        if blAnimationData.action != blAction:
            if blAnimationData.action is not None:
                raise RuntimeError("blAnimationData.action already assigned with a wrong value ?...")
            blAnimationData.action = blAction

        if attachToData:
            # also add an empty action to the object itself, Korman needs it.
            if targetBlenderObject.animation_data is None:
                targetBlenderObject.animation_data_create()
            if targetBlenderObject.animation_data.action is None:
                dummyActionName = self.blObjAnimationChannels.get(targetBlenderObject)
                if dummyActionName is None:
                    dummyActionName = targetBlenderObject.name + "_dummy"
                dummyAction = bpy.data.actions.get(dummyActionName)
                if dummyAction is None:
                    dummyAction = bpy.data.actions.new(dummyActionName)
                targetBlenderObject.animation_data.action = dummyAction

        return (targetBlenderObject, blAction, boneDataPathPrefix, bone)

    def setupKormanModifier(self, blObj, isAtc, isGlobalAnim, anim, importedFCurves):
        if not blObj.plasma_modifiers.animation.enabled:
            blObj.select = True
            bpy.context.scene.objects.active = blObj
            bpy.ops.object.plasma_modifier_add(types="animation")
            blObj.select = False
        kormanAnim = blObj.plasma_modifiers.animation

        defaultAnimName = "(Entire Animation)"
        isDefaultAnim = anim.name == defaultAnimName
        kSubAnim = None
        if isDefaultAnim:
            kSubAnim = next((a for a in kormanAnim.subanimations.animations if a.animation_name == defaultAnimName))
        else:
            kSubAnim = next((a for a in kormanAnim.subanimations.animations if a.animation_name == anim.name), None)
            if kSubAnim is None:
                kSubAnim = kormanAnim.subanimations.animations.add()
                kSubAnim.animation_name = anim.name

        looping = False
        if isAtc:
            kSubAnim.auto_start = anim.autoStart
            kSubAnim.loop = looping = anim.loop
        elif isGlobalAnim:
            kSubAnim.auto_start = True
            kSubAnim.loop = looping = True
            kSubAnim.sdl_var = anim.globalVarName
        frameMultiplier = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base
        if not isDefaultAnim:
            kSubAnim.start = round(anim.start * frameMultiplier)
            kSubAnim.end = round(anim.end * frameMultiplier)

        # markers
        def setMarker(name, time):
            marker = blObj.animation_data.action.pose_markers.get(name)
            if marker is None:
                marker = blObj.animation_data.action.pose_markers.new(name)
            marker.frame = round(time * frameMultiplier)
        if not isGlobalAnim:
            if anim.initial >= 0:
                setMarker("Initial", anim.initial)
            if (anim.loopStart - anim.start) > 0.001:
                setMarker("LoopStart", anim.loopStart)
            if (anim.loopEnd - anim.end) > 0.001:
                setMarker("LoopEnd", anim.loopEnd)

            if len(anim.loops) > 0:
                print("WARNING: animation loops not supported yet !")

            if len(anim.markers) > 0:
                print("WARNING: animation markers not supported yet !")

        if looping and isDefaultAnim:
            # add a loop modifier to animation curves, so we see those loop in the viewport
            for fcurve in importedFCurves:
                loopModifier = fcurve.modifiers.new(type='CYCLES')
